\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC2451 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 9 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\CNOT}{CNOT}

\begin{document}

\maketitle

\section*{Encoding Information Into Phases}

Remember the Hadamard gate, which for \(x \in \{0, 1\}\)
\[H\ket{x} = \frac{1}{\sqrt{2}}\left(\ket{0} + (-1)^x\ket{1}\right)\]
Last time, we saw that, for \(x = x_1...x_n\),
\[H^{\otimes n}\ket{x} = \frac{1}{\sqrt{2^n}}y \in \{0, 1\}^n(-1)^{x \cdot y}\ket{y}\]
where \(x \cdot y = \sum_ix_iy_i \mod 2\). This is an equal sum of all possible states, with the original state itself encoded in the phase of individual possibilities. We can also use the Hadamard gate to \textit{decode} phase information, since, in general,
\[HH\ket{\psi} = I\ket{\psi} = \ket{\psi}\]
implying that, for a complicated superposition, we have
\[H^{\otimes n}\frac{1}{\sqrt{2^n}}\sum_{y \in \{0, 1\}^n}(-1)^{x \cdot y}\ket{y} = \ket{x}\]
Now, this \((-1)^{x \cdot y}\) case is cool, but is a really special case. So let's think about how we can decode phases from very large superpositions looking like
\[\ket{\psi} = \frac{1}{\sqrt{2^n}}\sum_{y = 0}^{2^n - 1}e^{2\pi iy\alpha}\ket{y} \in \comps^{2^n}\]
where we think of \(y\) as an integer, and \(\alpha \in (0, 1)\).

\section*{The (State) Phase Estimation Problem}

In this problem, we're given as input a state \(\ket{\psi}\) as above for some \(\alpha\), and the goal is to recover \(\alpha\), or at least some very good approximation of it. So how do we do this?

First, let's think of \(\alpha\) in it's binary representation:
\[\alpha = 0.\alpha_1...\alpha_t = \sum_{i = 1}^t\frac{1}{2^i}\alpha_i\]
for \(\alpha_j \in \{0, 1\}\) for \(t\) bits of precision. Note any number can be written to arbitrary precision in this way. So let's start with some simple cases:
\begin{itemize}

  \item Assume \(t = 1, n = 1\), i.e. \(\alpha = 0.\alpha_1\). Then
  \[\ket{\psi} = \frac{1}{\sqrt{2}}\sum_{y = 0}^1e^{2\pi i\alpha y}\ket{y} = \frac{1}{\sqrt{2}}\left(\ket{0} + e^{2\pi i(0.\alpha_1)\ket{1}}\right)\]
  If \(\alpha_1 = 0\), this state is \(\ket{+}\), otherwise, the state is \(\ket{-}\). So the decoding problem is not too hard in this case: we have
  \[H\ket{\psi} = \ket{\alpha_1}\]

  \item So let's generalize, and assume we have \(t = n, n \geq 2\). We have
  \[\ket{\psi} = \frac{1}{\sqrt{2^n}}\sum_{y = 0}^{2^n - 1}e^{2\pi i(0.\alpha_1...\alpha_n)y}\ket{y} = \bigotimes_{k = 1}^n\frac{\ket{0} + e^{2\pi i(2^{n - k}\alpha)}\ket{1}}{2}\]
  So, for \(n = 2\), since
  \[2\alpha = 2\left(\frac{\alpha_1}{2} + \frac{\alpha_2}{4}\right) = \alpha_1 + \frac{\alpha_2}{2}\]
  we have
  \[\ket{\psi} = \left(\frac{\ket{0} + e^{2\pi i\alpha_2/2}\ket{1}}{\sqrt{2}}\right) \otimes  \left(\frac{\ket{0} + e^{2\pi i(\alpha_1/2 + \alpha_2/4)}\ket{1}}{\sqrt{2}}\right)\]
  So if we apply \(H\) to the first qubit, we'll get \(\alpha_2\). We want to find the value of \(\alpha_1\), but there's interference from \(\alpha_2\), which we need to get rid of.
  \begin{itemize}

    \item If \(\alpha_2 = 0\), there is no error, so we can just apply a Hadamard and measure \(\alpha_1\)

    \item If \(\alpha_2 = 1\), we need to rotate the second qubit to get rid of the \(\alpha_2/4\). We use a ``phase rotation gate": a control gate
    \[R_2^{-1} = \begin{pmatrix}
      1 & 0 \\ 0 & e^{-2\pi i/4}
    \end{pmatrix}\]
    We can do the math, and see that this gets rid of the \(\alpha_2/4\) component from the above, giving a pure \(\ket{+}\) or \(\ket{-}\) state depending on \(\alpha_1\) which we can then measure using a Hadamard gate.

    In general, we do the same thing: for a state
    \[\ket{\psi} = \left(\frac{\ket{0} + e^{2\pi i\alpha_n/2}\ket{1}}{\sqrt{2}}\right) \otimes \ket{\varphi}\]
    we measure \(\alpha_n\), and, after applying either \(I\) or \(R_n^{-1} = \begin{pmatrix}
      1 & 0 \\ 0 & e^{-2\pi i/2^n}
    \end{pmatrix}\)
    we recursively continue on the rest of the state, generated by \(\alpha' = 0.\alpha_1...\alpha_{n - 1}\).

    If we add up all the gates, this takes \(\mc{O}(n^2)\) gates, which is pretty cool considering we're extracting phase information from a vector living in a space of dimension \(2^n\).

    \item We'll leave out the case for \(t > n\) since this requires some unimportant (for now) details.

  \end{itemize}
  Now, this entire circuit forms one big unitary transformation. But that means that we can invert it, a.k.a. run the inverses of the gate in reverse order. This gives us a circuit for performing this unitary operation:
  \[\ket{x} \mapsto \frac{1}{\sqrt{2^n}}\sum_{y = 0}e^{2\pi iyyx/2^n}\ket{y}\]
  This transformation is called the \textbf{Quantum Fourier Transform}. It's extremely important: it lets us do Hamiltonians, to simulate quantum systems, and forms the backbone of Shor's algorithm.

\end{itemize}

\end{document}
