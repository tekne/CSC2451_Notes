\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC2451 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 11 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\begin{center}
\textbf{Note:} some material in these notes is, with the professor's permission, taken from the slides.
\end{center}

\section*{Introduction}

Quantum computing is computation, or more generally information processing, based off the principles of quantum mechanics. Quantum mechanics is a description of nature formulated in the twentieth century to explain the behaviour of subatomic particles. It has been spectacularly successful at explaining microscopic phenomena, and is called the most successful physics theory we've had so far.

It's been a hundred years since quantum mechanics was discovered, and yet the best classical methods we have for predicting the behaviour of a general quantum system require exponential time. That is, if we have a quantum system containing \(n\) particles, we require at least \(2^n\) (complex) numbers to describe the state of the system. That means that, for \(n \sim 300\) (the number of particles in a single uranium atom), we have that
\[2^n \gg 10^{82} \text{ (the number of atoms in the observable universe) }\]
This is kind of crazy: how can a simple thing, a single atom, have such a complex description. This is the starting point for why it seems like quantum physics seems to give us something ``beyond" classical physics: nature seems to be doing extravagant amounts of computation ``behind the scenes."

This gives us the natural question: if we can't describe anything beyond a handful of electrons, how do we do quantum physics? Well, there's three answers:
\begin{enumerate}

  \item 100 years of very clever approximations, e.g. Bethe ansatz, Feynman diagrams, perturbation theory, mean field approximations, etc.

  \item Computer simulations: density functional theory, Quantum Markov Chain Monte Carlo, etc. People use large amounts of supercomputing time to perform quantum mechanical calculations. Even with the best systems we have, we can't handle anything too complicated.

  \item Studying systems that allow the above answers!
  \begin{itemize}

    \item Systems that are mostly classical, where Newtonian mechanics is a good approximation

    \item Systems without strongly interacting particles

  \end{itemize}

\end{enumerate}
This story can be thought of as someone looking for their keys under the ``spotlight" of classical systems, whereas really they're someone else. There's this huge universe of non-classical systems where things behave in very different ways, like superconductors. Simulating general quantum mechanics is still a very hard task.

Some people realized this in the early 80's, specifically Richard Feynman, who had a talk called ``Simulating Physics with Computers." The question was simple: ``can probabilistic computers simulate quantum mechanics?" Instead of using computers based off classical mechanics, he had the idea of building a computer which took advantage of quantum mechanical elements. His motivation was the most obvious one: to simulate quantum systems, by actually simulating quantum systems. To a physicist, this makes sense. To a computer scientist in the 80's, or even today, this could be considered a ``crackpot" idea.

One of the founders of this idea, David Deutsch, found a problem which could be computed a constant factor faster by a quantum computer. A UofT PhD student, Dan Simons, found a problem which could be solved exponentially faster. Then Peter Shor, inspired by Simons' algorithm, realized the same idea could be applied to create an efficient quantum algorithm for a much more practical problem: integer factorization. That is, given an integer \(n > 0\), find an integer \(m > 1\) such that \(n | m\).

The security of the RSA cryptosystem \textit{depends} on this being a hard problem, so this is a big deal. It also puts us at a crossroads, because there are now three possibilities, and \textit{one} of them must be true:
\begin{enumerate}

  \item Quantum mechanics is wrong

  \item There is a fast classical algorithm for integer factorization

  \item Quantum computers are more powerful than classical computers

\end{enumerate}
Each one of these options is a pretty remarkable thing. Quantum mechanics is \textit{the} most successful scientific theory ever devised, with no disagreement ever found. Option number 2 sounds more plausible, but the ``Gauss argument" applies: if Carl Gauss didn't find it, it probably doesn't exist. A more serious argument that could be made is that the RSA cryptosystem has been out for fifty years, and there is enormous economic pressure to discover weaknesses if any exist: thousands of mathematicians at the NSA have been toiling at it for years. Finally we come to option number 3: that quantum computers are more powerful than classical ones. This would refute something called the Extended Church-Turing Thesis.

The original Church-Turing thesis says that Turing machines can simulate any effective model of computation. The extended Church-Turing thesis says that not only can we simulate any computational process, but we can do it \textit{efficiently}, i.e. we could design a Turing machine to effectively simulate anything that's going on in some computation device. But if a quantum computer implement's Shor's algorithm, we don't believe a classical computer could simulate that efficiently.
Any one of these options would be extremely fascinating, and of course, any one of them could be true.

On the practical side, steady progress has been made in quantum computing, both in designing hardware and algorithms. There are deep connections between quantum computing and both fundamental physics and pure mathematics, as well as of course quantum information theory, complexity theory, cryptography and more.

IBM, Google and Microsoft have put many resources into building scalable computers. Startups and academic labs like Rigetti Computing, Xanadu.ai (right across the street in Toronto), IonQ, Dwave, as well as Harvard, Waterloo, Maryland, Berkeley have also been working on the lastest advances. All of these labs are ``betting on different horses:" different methods for making quantum computing possible. What's currently popular is what's called ``superconducting qubits". You can actually submit programs to be run on IBM's two 5-qubit and one 16-qubit quantum computers. Google has built a 9-qubit quantum computer in 2016, and announced a 72-qubit quantum computer called Bristlecone, but has yet to publish results.

Microsoft has instead been betting on ``topological qubits", which are not yet known to exist, but promise a natural way to implement fault tolerant qubits and gates. These depend on the existence of ``non-abelian anyons", quasiparticles with exotic topological properties.

So, in summary, there's a ``qubit race" going on, and yet qubit count is not everything. Current devices are \textit{incredibly noisy}, so it's not just how many qubits you have, but how well they prerform. Scaling up and adding qubits is a tough engineering tabits, but there are no known fundamental obstacles to building a quantum computer with, say \(10^6\) noiseless qubits, which would allow us to use Shor's algorithm to factor numbers we cannot factor today.

According to John Preskill, we are living in the ``NISQ" era, standing for ``Noisy Intermediate-Scale Quantum," consisting of noisy devices with \(\sim 100\) qubits. They're not capable of running Shor's algorithm, but should still be capable of solving hard and interesting problmes. It may be that we're living in similar times as those who built the first transistors and analog computers.

We believe that quantum computers can give us exponential speedup for structured, algebraic problems, such as
\begin{itemize}

  \item Factoring (Shor's algorithm), which is an instance of the

  \item Hidden subgroup problem, which we don't think we can solve efficiently on classical compters

\end{itemize}
We also expect to find polynomial speedups for unstructured search problems. For example, assume we have a list of \(n\) items. In the worst case, we need to access \(n\) items in the database to find an item, but a quantum algorithm, Grover's algorithm, only requires \(\sqrt{n}\) time.
Another place we have exponential speedups is for quantum simulation, including of quantum physics and chemistry. This is probably the most important application of quantum computers so far, considering about 50\% of the supercomputing time today is being used to simulate the weather, and the other 50\% is devoted to chemistry, materials science and biology. People would be very, very interested in efficient ways of doing the latter. We're still, however, very far from implementing them from the quantum computers of today.

In the near term, quantum computers have been used to develop ``variational eigensolvers," and much work has been done on classical-quantum hybrid algorithms. Quantum Machine Learning is also an emerging field, including linear system solvers/SDPs/convex optimizers, quantum neural networks and recommendation systems. There remain, however, many open questions remaining to understand what types of problems can be solved faster using quantum computers.

Aside from computation, there are some connections to fundamental physics to explore. There are two successful theories in physics: quantum mechanics and general relativity. One works on the nanoscale, and the other on a cosmological scale, and yet we know these two theories are not the ``correct theory of nature," since they don't work on each \textit{other's} scale. Physicists want a theory of ``quantum gravity," and there is some hope that concepts in quantum computing, such as quantum error correcting codes and entanglement, could be key to a theory of ``quantum gravity," such that gravity could even be an emerging phenomenon of error correcting code. There is also the ``Black Hole Firewall Paradox", a paradox involving throwing a quantum computer into a black hole, and possible resolutions involve quantum cryptography. So this is an interesting question: what can quantum computing tell us about nature.

This is totally uncharted territory: we're at the early beginnings of a new field, and there's very exciting stuff out there we can't even imagine today. So let's get going.

\section*{Quantum Information Basics}

\subsection*{Bits and Qubits}

The simplest classical system is a \textit{bit}: two distinguishable states, 0 and 1. The simplest quantum system is a \textit{qubit}: mathematically represented as a complex unit vector
\[\ket{\psi} = \begin{pmatrix} \alpha \\ \beta \end{pmatrix} \in \mbb{C}^2, |\alpha|^2 + |\beta|^2 = 1\]
A qubit can represent classical 0 and 1 states:
\[\ket{0} = \begin{pmatrix} 1 \\ 0 \end{pmatrix}, \ket{1} = \begin{pmatrix} 0 \\ 1 \end{pmatrix}\]
Every qubit \(\ket{\psi}\) can be written as a linear combination of \(\ket{0}\) and \(\ket{1}\), i.e. is a superposition of these two states, given by
\[\ket{\psi} = \alpha\ket{0} + \beta\ket{1} = \begin{pmatrix} \alpha \\ \beta \end{pmatrix}\]
The \(\ket{\psi}\) notation is called \textit{Dirac notation}, and is used to represent quantum states. Mathematically the ``ket vector" \(\ket{\psi}\) is a column vector. The \textit{dual/Hermitian conjugate} of column vectors, that is, row vectors, are called ``bra vectors". We have
\[\ket{\psi} = \begin{pmatrix} \alpha \\ \beta \end{pmatrix} \implies \bra{\psi} = \begin{pmatrix} \alpha^* \\ \beta^* \end{pmatrix} = \alpha^*\ket{0} + \beta^*\ket{1}\]
Note \(\alpha^*, \beta^*\) denote the complex conjugates of \(\alpha, \beta\). The inner product between two ket vectors is hence given by
\[\braket{\psi|\varphi}\]
The naming comes from the fact that ``bracket = bra + ket".

Now, how can we know a qubit? If we try, we run into one of the fundamental postulates of quantum mechanics: quantum states are not directly accessible, but must be \textit{measured}.

If we measure a qubit \(\alpha\ket{0} + \beta\ket{1}\), we obtain a \textit{classical} result \(j \in \{0, 1\}\) probabilistically. Specifically, there is a \(|\alpha^|2\) probability we measure \(\ket{0}\), and a \(|\beta|^2\) probability we measure \(\ket{1}\). If we measure the qubit again, we will obtain \(\ket{j}\) with \textit{certainity}, i.e. the qubit has collapsted to a classical state, and the superposition has been destroyed. The takeaway from this is that quantum information is fragile. We may have \(2^{300}\) pieces of quantum information, but the moment you measure it, it's gone!

So, how does a qubit evolve? Another postulate of quantum mechanics is that quantum states in isolation, i.e. when not measured, evolve via \textit{unitary operators}. A unitary operator acting on a qubit can be described by a matrix
\[U = \begin{pmatrix} a & b \\ c & d \end{pmatrix}\]
which takes unit vectors to unit vectors. Equivalently, we can say it preserves inner products, i.e.
\[\ket{\psi'} = U\ket{\psi} \land \ket{\varphi'} = U\ket{\varphi} \implies \braket{\psi|\varphi} = \braket{\psi'|\varphi'}\]

So how is a quantum bit different from a probabilistic bit? To illustrate this difference, let's think about two experiments:
\begin{itemize}

  \item Experiment A: let \(\ket{\psi} = \ket{0}\) and \(U = \frac{1}{\sqrt{2}}\begin{pmatrix} 1 & -1 \\ 1 & 1 \end{pmatrix}\). We
  \begin{itemize}

    \item Apply \(U\) to the qubit, giving \(U\ket{0} = \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\).

    \item Measure the qubit, giving outcomes \(a\) and \(b\): \(\ket{0}\) and \(\ket{1}\), with equal probability

    \item Apply \(U\) to the qubit. In outcome \(a\), we have quantum state \(\frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\), but in outcome \(b\), we have quantum state \(\frac{1}{\sqrt{2}}\ket{0} - \frac{1}{\sqrt{2}}\ket{1}\)

    \item Measure the qubit. Again, no matter what the outcome was in step 2, the possibilities are the same

  \end{itemize}
  This is pretty boring, the point is that this is looks like a standard random bit. So let's try another experiment: let's skip the first measurement.

  \item Experiment B: let \(\ket{\psi} = \ket{0}\) and \(U = \frac{1}{\sqrt{2}}\begin{pmatrix} 1 & -1 \\ 1 & 1 \end{pmatrix}\).
  \begin{itemize}

    \item We apply \(U\) to the qubit twice, and we find that we first have
    \[U\ket{0} = \frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\]
    and then have
    \[U\left(\frac{1}{\sqrt{2}}\ket{0} + \frac{1}{\sqrt{2}}\ket{1}\right) = \frac{1}{\sqrt{2}}(U(\ket{0}) + U(\ket{1})) = \ket{1}\]

    \item So when we measure, we get, with certainty, \(\ket{1}\).

  \end{itemize}
  The final outcome here is deterministic. This is an example of \textit{interference} in QM, since the zero states cancel out. The takeaway here is that minus signs in the amplutudes matter. Multiple unitary operations give rise to a tree of ``paths" between states, and each path has an amplitude associated with it. Before measurement, paths can constructively or destructively interfere with each other, whereas in classical probability theory paths are always positive and can only add.


  Intermediate measurements destroy superpositions, however, and hence prevent interference. Hence, if you want to see interesting quantum effects, you have to delay measurements as long as possible. This is why we rarely see quantum effects in everyday life, as measurement is constantly happening: a stray cosmic ray entering your system counts as a measurement.

\end{itemize}

\subsection*{Composite Quantum Systems}

The state of a qubit lives in the vector space \(\mbb{C}^2\). This is called the \textit{Hilbert space} of a qubit. In the finite-dimensional case, which is enough for our use-case, a Hilbert space is just a complex vector space with an inner product.

The Hilbert space of 2 qubits is the \textit{tensor product space} \(\mbb{C}^2 \otimes \mbb{C}^2\). \(\mbb{C}^2\) has an orthonormal basis \(\{\ket{0}, \ket{1}\), and the tensor product space \(\mbb{C}^2 \otimes \mbb{C}^2 \simeq \mbb{C}^4\), with orthonormal basis
\[
  \ket{0} \otimes \ket{0} = \ket{00} =  \begin{pmatrix} 1 \\ 0 \\ 0 \\ 0 \end{pmatrix}, \qquad
  \ket{0} \otimes \ket{1} = \ket{01} = \begin{pmatrix} 0 \\ 1 \\ 0 \\ 0 \end{pmatrix}, \qquad
  \ket{1} \otimes \ket{0} = \ket{10} = \begin{pmatrix} 0 \\ 0 \\ 1 \\ 0 \end{pmatrix}, \qquad
  \ket{1} \otimes \ket{1} = \ket{11} = \begin{pmatrix} 0 \\ 0 \\ 0 \\ 1 \end{pmatrix}, \qquad
\]
Note that \(\ket{ij}\) is shorthand for \(\ket{i} \otimes \ket{j}\).

The tensor product of vectors is defined by, if \(\ket{\psi} = \alpha\ket{0} + \beta\ket{1}\) and \(\psi = \gamma\ket{0} + \delta\ket{1}\),
\[\ket{\psi} \otimes \ket{\varphi} = (\alpha\ket{0} + \beta\ket{1}) \otimes (\gamma\ket{0} + \delta\ket{1})
= \alpha\gamma\ket{00} + \alpha\delta\ket{01} + \beta\gamma\ket{10} + \beta\delta\ket{11}
= \begin{pmatrix} \alpha\gamma \\ \alpha\delta \\ \beta\gamma \\ \beta\delta \end{pmatrix}\]
In general, a two-qubit state \textbf{cannot} be written as a tensor product state, i.e.
\[\forall \varphi, \theta, \ket{\psi} \neq \ket{\varphi} \otimes \ket{\theta}\]
States that cannot be written in product form are called \textit{entangled}. Otherwise, they are called \textit{unentangled}. We will talk more about this next class.

Taking inner products in \(\mbb{C}^2 \otimes \mbb{C}^2\), if \(\ket{a}, \ket{b}, \ket{c}, \ket{d} \in \mbb{C}^2\), we have
\[(\ket{a} \otimes \ket{b})(\ket{c} \otimes \ket{d}) = \braket{a|c} \cdot \braket{b|d}\]

If we measure a two-qubit state
\[\ket{\psi} = \sum\alpha_{ij}\ket{ij} \in \mbb{C}^2 \otimes \mbb{C}^2\]
we obtain a classical outcome \((i, j) \in \{0, 1\}^2\) with probability \(|\alpha_{ij}|^2\). In the case of partial measurements, we naturally obtain a classical outcome \(i \in \{0, 1\}\) with the marginal probability
\[p_i = \sum_j|\alpha_{ij}|^2\]
The post-measurement state in this case is then
\[\ket{\psi_i} = \frac{1}{\sqrt{p_i}}\sum_j\alpha_{ij}\ket{ij} = \ket{i} \otimes \frac{1}{\sqrt{p_i}}\sum_j\alpha_{ij}\ket{j}\]
Note the factor of \(\frac{1}{\sqrt{p_i}}\) is to re-normalize the vector. As an exercise, show that doing a partial measurement on one qubit, followed by a partial measurement on the second, gives the same distribution of outcomes as doing a full measurement the first time around.

In this case, evolution is basically the same: two-qubit systems in isolation undergo evolution via unitary operators acting on \(\mbb{C}^2 \otimes \mbb{C}^2\). Remember that unitary operators preserve the \(\ell_2\)-norm/preserve inner products/is inverse to its conjugate transpose!

More generally, if we have orthonormal bases \(\{\ket{u_i}\}, \{\ket{v_j}\}\) for \(\mbb{C}^r\), \(\mbb{C}^s\), then the tensor product space \(\mbb{C}^r \otimes \mbb{C}^s\) is \(r \times s\) dimensional and has orthonormal basis \(\{\ket{u_i} \otimes \ket{v_j}\}\).

\subsection*{The No-Cloning Theorem}

Classical bits are easy to copy, but quantum information is different. The No-Cloning theorem can be, in a sense, worded as ``there is no quantum Xerox machine". More formally, there is no unitary operator \(U\) acting on two qubits such that
\[U(\ket{\psi} \otimes \ket{0}) = \ket{\psi} \otimes \ket{\psi}\]
(the \(\ket{0}\) is here called an \textit{ancilla qubit}).

To prove this, we use the fact that any ``quantum Xerox machine" must act as a ``classical Xerox machine", and yet ``classical Xerox machines" cannot copy general quantum states.

Consider the two-qubit classical copying unitary transformation described by
\[U\ket{x0} = \ket{xx}, U\ket{x1} = \ket{x} \otimes \ket{x \oplus 1}\]
which can be written as
\[
U = \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 0 & 1 \\ 0 & 0 & 1 & 0 \end{pmatrix}
\]
This unitary is called CNOT, or controlled not, and has
\[U(\ket{x} \otimes \ket{0}) = \ket{x} \otimes \ket{x}, U(\ket{x} \otimes \ket{1}) = \ket{x} \otimes \ket{x \oplus 1}\]
If we try to copy
\[\ket{\psi} = \frac{1}{\sqrt{2}}(\ket{0} + \ket{1})\]
using \(U\), we get
\[U\ket{\psi} \otimes \ket{0} = \frac{1}{\sqrt{2}}(U\ket{00} + U\ket{10}) = \frac{1}{\sqrt{2}}(\ket{00} + \ket{11}) \neq \ket{\psi} \otimes \ket{\psi}\]
So what we've shown is that the one ``classical copy machine" transform has a state (in this case, \(\psi\) is a very important state known as the EPR pair, or Bell pair, or maximally entangled state) which it cannot duplicate.

\subsection*{The Exponentiality of QM}

We now come to a third principle of quantum mechanics: if the states of systems \(A\) and \(B\) are \(\ket{\psi} \in \mbb{C}^r\) and \(\ket{\varphi} \in \mbb{C}^s\), respectively, then the joint state of \(AB\) is
\[\ket{\psi} \otimes \ket{\varphi} \in \mbb{C}^r \otimes \mbb{C}^s\]
This implies the size of joint states grows exponentially with the number of particles. We can hence conclude that nature is doing an incredible amount of work to ``keep track of all this." There's a tension here, however, with the fact that all of this extravagant computation is hidden behind the ``veil of measurement," and we can only access this information in a limited way. This is what makes quantum computation nontrivial. There's an important theorem that captures this tension:
\begin{theorem}[Holevo's Theorem]
One cannot reliably store \(n\) bits of classical information in less than \(n\) qubits.
\end{theorem}
Formally, let \(m < n\). If Alice gets a uniformly random string \(X \in \{0, 1\}^m\), and sends an \(m\)-qubit state \(\ket{\psi_X}\) to Bob, who applies a measurement to obtain a string \(Y \in \{0, 1\}^n\), we have that for any encoding/decoding strategy Alice and Bob use,
\[I(X:Y) \leq m\]

\end{document}
