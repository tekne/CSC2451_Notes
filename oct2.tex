\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC2451 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{October 2 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\DeclareMathOperator{\CNOT}{CNOT}

\begin{document}

\maketitle

\section*{Deutsche's Algorithm}

Consider a function \(f: \{0, 1\} \to \{0, 1\}\). We want to know whether \(f(0) = f(1)\). Classically, this requires two queries to solve, but a quantum algorithm can solve this with one query. Our access to \(f\) is via an oracle, specifically an XOR oracle, taking an input \(\ket{x}\) which is left unchanged and an input \(\ket{b}\) which becomes \(\ket{b \oplus f(x)}\). We could also use a phase oracle taking \(\ket{x}\) to \((-1)^{f(x)}\ket{x}\), and we leave it to the homework to show that XOR oracles and phase oracles are equivalent.
So here's the algorithm: starting with two \(\ket{0}\) qubits,
\begin{itemize}

  \item Take the first qubit and put it through a Hadamard gate

  \item Take the second qubit, put it through the \(\sigma_x\) gate, and then through a Hadamard gate

  \item Set the first qubit as the input and the second qubit as the target to the XOR oracle for \(f\)

  \item Put the first qubit through a Hadamard gate

  \item Measure the first qubit. This is the result.

\end{itemize}
After each stage, we have states
\begin{itemize}

  \item \(\psi_0 = \ket{0} \otimes \ket{0}\)

  \item \(\psi_1 = \ket{0} \otimes \ket{1}\)

  \item \(\psi_2 = \ket{+} \otimes \ket{-}\)

  \item \(\psi_3 = \frac{1}{2}\left(
    \ket{0} \otimes (\ket{f(0)} + \ket{\overline{f(0)}} + \ket{1} \otimes (\ket{f(1)} - \ket{\overline{f(1)}})
  \right)\)

  \item \(\psi_4 = (H \otimes I)(\psi_3) = \frac{1}{2\sqrt{2}}\left(
    \ket{0} \otimes (\ket{f(0)} - \ket{\overline{f(0)}} + \ket{f(1)} - \ket{\overline{f(1)}}) + \ket{1} \otimes (\ket{f(0)} - \ket{\overline{f(0)}} - \ket{f(1)} + \ket{\overline{f(1)}})
  \right)\)

\end{itemize}
If we plug in values for \(f(0), f(1)\) into the above, we see that the algorithm indeed perfectly solves the given decision problem. Note also that we can shorten this circuit by just taking the initial state to be \(\ket{+} \otimes \ket{-}\).
Also note that we can't have the oracle \(\ket{x} \mapsto \ket{f(x)}\), because in general this is not a unitary operator.

\section*{Simon's Algorithm}

Let \(f: \{0, 1\}^n \to \{0, 1\}^n\) provided that,
\[\forall x \neq y \in \{0, 1\}^n, f(x) = f(y) \iff x = y \oplus s\]
where \(s \in \{0, 1\}^n\) is a ``secret string". Our goal is, given black-box access to \(f\), to find this string.
Clasically, the only way to find \(s\) is to find a collision \(f(x) = f(y)\), which takes exponentially many queries to \(f\). However, there exists a quantum algorithm solving this with a polynomial time query. How this works should look very familiar, in fact, it's almost exactly the same as Deutsche's algorithm.

The circuit is as follows: we're going to start with two bunches of \(n\) qubits all set to zero, called registers. We'll write them \(\ket{0}^{\otimes n}\), giving state \(\ket{0}^{\otimes n} \otimes \ket{0}^{\otimes n}\). We then define a circuit on these registers doing the following:
\begin{itemize}

  \item Apply the Hadamard gate to every bit of the first register.

  \item Put the first register into the (XOR) oracle for \(f\) as the input and the second as the target

  \item Apply the Hadamard gate to every bit of the first register.

  \item Measure the first register.

\end{itemize}
This circuit is used as a subroutine. So, let's see what happens to the state each stage:
\begin{itemize}

  \item \(\ket{\psi_0} = \ket{0}^{\otimes n} \otimes \ket{0}^{\otimes n}\)

  \item \(\ket{\psi_1} = (H^{\otimes n}\ket{0}^{\otimes n}) \otimes \ket{0}^{\otimes n} = (H\ket{0})^{\otimes n} \otimes \ket{0}^{\otimes n}
  = \ket{+}^{\otimes n} \otimes \ket{0}^{\otimes n}\). We have
  \[
    \ket{+}^{\otimes n}
    = \frac{1}{\sqrt{2^n}}(\ket{0} + \ket{1})^{\otimes n}
    = \frac{1}{\sqrt{2^n}}\bigotimes_{i = 1}^n\left(\sum_{x_i \in \{0, 1\}}\ket{x_i}\right)
    = \frac{1}{\sqrt{2^n}}\sum_{x \in \{0, 1\}^n}\ket{x_1...x_n}
  \]
  So we can think of this as a uniform probability distribution, i.e. superposition, over all \(n\)-bit strings. Putting it through the oracle, we have ``all the data about \(f\)" in superposition. Specifically, we have

  \item \(
    \ket{\psi_2} = \mc{O}_f\ket{\psi_1}
    = \frac{1}{\sqrt{2^n}}\sum_{x \in \{0, 1\}^n}\mc{O}_f\ket{x}\ket{0}^{\otimes n}
    = \frac{1}{\sqrt{2^n}}\sum_{x \in \{0, 1\}^n}\ket{x}\ket{f(x)}
  \)
  We have that
  \[
    H^{\otimes n}\ket{x_1...x_n} = \bigotimes_{i = 1}^n\frac{1}{\sqrt{2}}(\ket{0} + (-1)^{x_1}\ket{1}) = \frac{1}{\sqrt{2^n}}\sum_{y \in \{0, 1\}^n}(-1)^{x \cdot y}\ket{y}
  \]
  Where \(x \cdot y = \sum_ix_iy_i \).
  Hence, we have

  \item \(
    \ket{\psi_3} = \frac{1}{2^n}\sum_{x \in \{0, 1\}^n}\sum_{y \in \{0, 1\}^n}(-1)^{x \cdot y}\ket{y}\ket{f(x)}
  \)

\end{itemize}
Now assume that, when measuring the first megister, we get an outcome \(y^*\). Then the un-normalized post-measurement state is going to be
\[
  \ket{y^*} \otimes \frac{1}{2^n}\sum_x(-1)^{x \cdot y^*}\ket{f(x)}
\]
To figure out the probability of \(y^*\), we just have to look at the norm squared
\[\left|\frac{1}{2^n}\sum_x(-1)^{x \cdot y}\ket{f(x)}\right|^2 = \frac{1}{2^n}\left|\sum_{b \in B}(-1)^{x_1 \cdot y} + (-1)^{x_2 \cdot y}\right|\]
where \(B\) is the image of \(f\). Expanding, we have that the above is equal to
\[
  \frac{1}{2^n}\left|\sum_{b \in B}\left((-1)^{x_1 \cdot y} + (-1)^{(x_1 + s) \cdot y}\right)\ket{b}\right|^2
  = \frac{1}{2^n}\left|\sum_{b \in B}(-1)^{x_1 \cdot y}\left(1 + (-1)^{s \cdot y}\right)\ket{b}\right|^2\]
\[
  = \frac{1}{2^{2n}}\left|\sum_{b \in B}(-1)^{x_1 \cdot y}\left(1 + (-1)^{s \cdot y}\right)\ket{b}\right|^2
  = \frac{1}{2^{2n}}\sum_{b \in B}\left(1 + (-1)^{s \cdot y}\right)^2 = \frac{2^{n - 1}}{2^{2n}}\left(1 + (-1)^{s \cdot y}\right)^2\]
  \[= \frac{1}{2^{n + 1}}\left\{\begin{array}{cc}
    0 & \text{if} \ s \cdot y \ \text{odd} \\
    4 & \text{if} \ s \cdot y \ \text{even}
  \end{array}\right.
\]
So when we measure we obtain \(y^*\) with probability \(0\) if \(s \cdot y = 1 \mod 2\), or \(\frac{1}{2^{n - 1}}\) if \(s \cdot y = 0 \mod 2\). So we're basically getting random strings \(y\) having inner product \(0\) with the target. How can we recover the secret?

Next time, we'll cover something called the Quantum Fourier Transform, which is used as the workhorse behind Shor's factoring algorithm.

\end{document}
