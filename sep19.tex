\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC2451 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{September 19 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathabx}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{tikz}
\usepackage{tikz-cd}
\usepackage{braket}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem*{claim}{Claim}
\newtheorem*{fact}{Fact}
\newtheorem*{notation}{Notation}
\newtheorem*{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\comps}[0]{\mathbb{C}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\cmt}[2]{\left[#1, #2\right]}
\newcommand{\field}[1]{\mathbb{F}_{#1}}
\newcommand{\intmod}[1]{\ints / #1 \ints}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\mbb}[1]{\mathbb{#1}}
\newcommand{\GL}[2]{\text{GL}_{#1}(#2)}
\newcommand{\defeq}[0]{:=}
\newcommand{\generate}[1]{\left\langle #1 \right\rangle}
\newcommand{\restrict}[2]{{#1}|_{#2}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}

\begin{document}

\maketitle

\section*{Review of Quantum Basics}

Quantum states are (unit) vectors \(\ket{\psi} \in \comps^d\) such that
\[\ket{\psi} = \sum_{i = 0}^{d - 1}\alpha_i\ket{i}\]
such that \(\sum_i|\alpha_i|^2 = 1\). Such a quantum state has \(d\) distinguishable quantum states. Quantum information is subject to three postulates:
\begin{enumerate}

  \item If we, as classical beings, want to know a quantum state, we have to measure it. Measuring collapses a quantum state \(\ket{\psi}\) to a classical state \(\ket{i}\) with probability \(|\alpha_i|^2\).

  Mathematically, measurement is projection to an orthonormal basis. Physically, we have some hairy, philosophical questions to answer, and people have written tons of books about it, as at least theoretically we too are quantum systems. Later in the course, when we talk about quantum circuits, there is a kind of answer, but for now, we'll take as an axiom that we are ``classical beings"

  \item Isolated quantum systems evolve via unitary maps (i.e. high dimensional rotations and reflections), in the form
  \[\ket{\psi'} = U\ket{\psi}\]

  \item If \(\ket{\psi} \in \comps^{d_1}, \ket{\varphi} \in \comps^{d_2}\) are quantum states, then their composite state is
  \[\ket{\psi} \otimes \ket{\varphi} \in \comps^{d_1} \otimes \comps^{d_2}\]
  which lives in a space of dimension \(d_1 \cdot d_2\).

\end{enumerate}

\subsection{Quantum Information Generalizes Classical Probability}

Everything else falls out as corollaries of these axioms. But let's give another perspective on this. While quantum information is different from classical information, and classical probability theory, we should really think of it as a kind of generalization of classical probability theories. We can build a kind of ``dictionary"
\begin{center}
\begin{tabular}{lr|lr}
\multicolumn{2}{c|}{Classical probability} & \multicolumn{2}{|c}{Quantum Information} \\ \hline
Probabilities: & \(0 \leq p \leq 1\) & Amplitudes: & complex \#s \quad \(|\alpha| \leq 1\) \\
Distributions: & \(\sum p_i = 1\) & Quantum states: & \(\sum|\alpha_i|^2 = 1\) \\
\multicolumn{2}{c|}{Taking a sample} & \multicolumn{2}{|c}{Measurement} \\
\multicolumn{2}{c|}{Stochastic matrices} & \multicolumn{2}{|c}{Unitary evolution} \\
\multicolumn{2}{c|}{Joint distribution} & \multicolumn{2}{|c}{Composite quantum systems} \\
\multicolumn{2}{c|}{Correlated} & \multicolumn{2}{|c}{Entangled} \\
\multicolumn{2}{c|}{Product distribution} & \multicolumn{2}{|c}{Product states} \\
\multicolumn{2}{c|}{Change of variables} & \multicolumn{2}{|c}{Measurement in different bases} \\
\multicolumn{2}{c|}{\textbf{Nothing!}} & \multicolumn{2}{|c}{Interference} \\
\end{tabular}
\end{center}
So this kind of dictionary can provide us a way to make things ``concrete." The whole ``interference" thing, however, has no analog, and people have spilled a lot of ink exploring this and the presumed exponential ``quantum speedup."

\subsection*{Measurement in Different Bases}

So one term from above we've hinted at but haven't yet explored is measurement in different bases. The idea is very simple: why is \(\ket{i}\) special? Previously, we've been measuring in a standard basis, but what if we had the space \(\comps^d\), and we chose our favorite orthonormal basis
\[B = \{\ket{v_i}\}\]
Someone walks up to us with a quantum state \(\ket{\psi} \in \comps^d\). In principle we can run an experiment where we measure according to this basis. When we measure in this basis, what we're going to get as an outcome is that the state collapses not to a standard basis vector but to one of these new basis vectors we came up with, that is,
\[\ket{\psi} \text{ collapses to } \ket{v_i} \text{ with probability  } |\braket{\psi|v_i}|^2\]
So when we measure, we're doing this projection, and the (squared) lengths along the projection are proportional to the probability of collapsing to a given projection. This is pretty fortunate, considering in real life there is no ``standard basis." Physically, measuring in a ``different basis" just means applying a rotation to move our other orthonormal basis to our standard basis (i.e. the reference frame of our equipment) and then measuring.
There's another formalism discussing non-orthonormal bases, but we won't talk about that in this course.

\subsection*{Partial Measurement}

We started talking about this last time, but to recap, a two qubit state \(\ket{\psi}\) can be written as
\[\ket{\psi} = \sum_{i, j \in \{0, 1\}}\alpha_{ij}\ket{i} \otimes \ket{j}\]
The classical analog is that with probability \(\alpha_{ij}\), the probability of the state of our two qubits being \(i, j\) is \(|\alpha_{ij}|^2\).

Let's say we wanted to focus on the state of the ``left" qubit, and let's say we wanted to measure it not in the standard basis, but in \(\{\ket{v_1}, \ket{v_2}\} \subseteq \comps^2\) (note this is a basis just for the \textit{left} qubit, \textit{not} the whole system). We have to have some way of describing what happens to the system as a whole after this measurement.

So when we measure the first qubit, in order to be consistent with the first two postulates, it must collapse into either \(\ket{v_1}\) or \(\ket{v_2}\). There's two remaining questions:
\begin{enumerate}

  \item With what probability does it collapse into either state

  \item What happens to the second qubit

\end{enumerate}
So we'll start with the second question: what's the post-measurement state of the second qubit? Mathematically, we want to ``project" the first qubit onto a particular outcome, say \(\ket{v_1} \in \comps^2\). So we want to take a ``partial inner product" with the vector \(\ket{\psi} \in \comps^4\). How do we do that? Well, we take the row vector \(\ket{v_1}\) and multiply on the left with \(\psi\), i.e.
\[\ket{\varphi'} = \ket{v_1}\left(\sum_{ij}\alpha_{ij}\ket{i} \otimes \ket{j}\right) = \sum_{ij}\alpha_{ij}\braket{v_1|i} \otimes \ket{j} \in \comps^2\]
This vector as a whole is the state of a single qubit, and \textit{up to normalization} is the post-measurement state of the second qubit, i.e. we have state
\[\ket{\varphi} = \frac{\ket{\varphi'}}{|\ket{\varphi'}|}\]
After this whole measurment has been done, assuming the outcome is \(\ket{v_1}\), the joint state is going to be \(\ket{v_1} \otimes \ket{\varphi}\).
So is the norm just useless information? No: because it actually tells us what the probability of measuring \(v_1\) in the first place is! Specifically, the probability of measuring outcome \(v_1\) for the first qubit is given by \(|\ket{\varphi'}|^2\)

\section*{The Heisenberg Uncertainty Principle}

This is a concept from physics that many of us have heard of. Colloquially, we often say that for any particle, we cannot know it's position and momentum at the same time. But what does this actually mean?

In quantum information terms, it means that a state \(\ket{\psi}\) (think of it as a qubit) cannot be simultaneously determined by two incompatible bases, i.e. two bases that are essentially not aligned with each other. For eample, let's think about the standard basis \(B_1 = \{\ket{0}, \ket{1}\), and let's think about another basis, the Hadamard basis \(B_2 = \{\ket{+}, \ket{-}\}\) given by
\[\ket{+} = \frac{1}{\sqrt{2}}(\ket{0} + \ket{1}), \ket{-} = \frac{1}{\sqrt{2}}(\ket{0} - \ket{1})\]
Observe that we can measure in \(B_2\) since \(\ket{+}, \ket{-}\) are orthogonal unit vectors. But suppose we had a qubit \(\ket{\psi} = \alpha\ket{0} + \beta\ket{1}\) which gets outcome \(\ket{i}\) in the \textit{standard basis} \(B_1\). This means that either \(\alpha = 0\) or \(\beta = 0\),
i.e. \(\ket{psi} = \ket{0} \text{ or } \ket{1}\).
Now imagine measuring \(\ket{\psi}\) in the Hadamard basis instead. It's pretty clear that there is no way in which we'd get \(\ket{+}\) or \(\ket{-}\) with certainy: in fact, we'd have about a 50/50 chance of getting either regardless of the case.

In other words, our result is completely determined in one basis \(B_1\) and completely random in another basis \(B_2\). This goes the other way around: something completely determined in the Hadamard basis \(B_2\) will be completely random in the standard basis \(B_1\).

Now, in real life, we can't know if our state is perfect, but if we have a whole bunch of copies of a constant state (e.g. prepared by an algorithm) and determine with certainty it's something in one basis, the more certain we are in one basis, the less we'll be in another. That's the uncertainty principle. Written out, if \(\Delta S\) is uncertainty in the standard basis, and \(\Delta H\) is uncertainty in the Hadamard basis, we here have
\[\Delta S\Delta H \geq \frac{1}{\sqrt{2}}\]

\section*{Entanglement}

Let \(\ket{\psi} \in \comps^2 \otimes \comps^2\). In general,
\[\lnot\exists \varphi_1, \varphi_2 \in \comps^2, \ket{\psi} = \ket{\varphi_1} \otimes \ket{\varphi_2}\]
If we \textit{cannot} write \(\ket{\psi}\) as the product of two states, we call it an \textit{entangled state}. One particularly important such state is the EPR (Einstein-Podolsky-Rosen) pair
\[\ket{\psi} = \frac{1}{\sqrt{2}}(\ket{00} + \ket{11})\]
As computer scientists, we like telling stories about Alice and Bob. So say two qubits form an EPR pair, Alice has one and Bob has the other. If they measure at the same time in the standard basis, they'll both have the same outcome, guaranteed. Each will get a randomly determined outcome, but it'll be the same one.

Now, this kind of correlation is not so impressive, since we can reproduce this kind of correlation in classical mechanics by, say, flipping a coin, duplicating it and putting it into two boxes. But there's a way to create a stronger kind of correlation which \textit{cannot} be reproduced with classical mechanics.

Consider a third character, the ``referee" (not Eve, since she's usually mean), who sends a classical, random bit \(x\) to Alice and \(y\) to Bob. Alice and Bob can't communicate, but both have two halves of an EPR pair.
Both have to somehow measure their qubit to obtain outcomes \(a, b \in \{0, 1\}\), and report their answers back to the referee. The referee will check if the following condition holds:
\[a + b \equiv xy \mod 2 \qquad \text{i.e.} \qquad a \oplus b = x \land y\]
If this holds, Alice and Bob win.
So, what is the maximum probability of winning? For classical players, i.e. without using any qubit, they can obtain a maximum probability of \(75\%\) by just, regardless of the question, replying with 0. This is what classical physics predicts: that's the best they can do.

But let's imagine instead they perform some more clever measurements on the EPR pair. Then they can win about than \(85\%\) of the time (they can actually win with \(\cos(\pi/8)^2\) probability). So what the EPR pair is giving them is a way to win with more probability than can be possible classically. Here's how:
\begin{itemize}

  \item If \(x = 0\), Alice will measure her qubit in the standard basis, and report the result to the referee. If \(x = 1\), she will measure the qubit using the Hadamard basis. If she gets \(\ket{+}\), she'll report 0, otherwise she'll report 1.

  \item If \(y = 0\), Bob will measure his cubit with the basis \(\{\ket{a_0}, \ket{a_1}\), where
  \[a_0 = \cos(\pi/8)\ket{0} + \sin(\pi/8)\ket{1}, a_1 = -\sin(\pi/8)\ket{0} + \cos(\pi/8)\ket{1}\]
  If he gets \(a_i\), he reports \(i\). On the other hand, if \(y = 1\), he will instead use the basis \(\{\ket{b_0}, \ket{b_1}\}\), where
  \[b_0 = \cos(\pi/8)\ket{0} - \sin(\pi/8)\ket{1}, b_1 = \sin(\pi/8)\ket{0} + \cos(\pi/8)\ket{1}\]

\end{itemize}
Now this is a weird strategy, but I claim that this is the best one. We can still pick other coefficients which also beat the classical probabilities, but it is the optimal strategy overall. Anyways, if you compute the probabilities, you'll find the probability of winning is indeed higher than \(75\%\), which is what's important.

Note the ``spooky action at a distance": it may be that Alice and Bob are so far from each other that, should the referee be between them and require that they send their answers back within a certain time, there could be no way from them to communicate any information between each other slower than light. So in a way, things are happening faster than light! Due to this, Einstein was very uncomfortable with the implications of the quantum theory.

Einstein, Podolsky and Rosen published a paper in 1935 arguing that quantum mechanics could not be a complete description of nature based off a version of this speed-of-light argument: assume Alice and Bob are very, very far away from each other, say in different galaxies or even opposite ``ends of the universe".
In Experiment A, Alice measures in the standard basis, she will get \(\ket{0}\) or \(\ket{1}\) with equal probability, causing the state of both of their qubits to collapse, even though Bob is at the ``other end of the universe." In Experiment B, Alice measures in the Hadamard basis, and again gets \(\ket{+}\) or \(\ket{-}\) with equal probability, and again both qubits collapse. Einstein didn't believe that it could be possible that effects on the fundamental state of the quantum systems involved could propagate faster than light, and yet, it violates the uncertainty principle that the state could be determined in both the standard and Hadamard basis.

Einstein hoped that there was a better classical theory that reproduced quantum mechanics, known as a local hidden variable theory, where the state of particles was stored in some variables which we did not know how to measure. In 1964, however, Bell proved that this hope is impossible, essentially with the game above: you can prove that in any local hidden variable model, playing this game could only get a \(75\%\) winning probability. Even better, even if you're a complete quantum skeptic, you can actually play this game in a lab and \textit{see} for yourself that the world \textit{cannot} be acting classically, as far as we know up to arbitrary precision.

\end{document}
